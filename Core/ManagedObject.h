#pragma once

using namespace System;

namespace OpenGL
{
    template<class T>
    public ref class ManagedObject
    {
    public:
        ManagedObject(T* instance)
            : Instance(instance)
        {
        }

        virtual ~ManagedObject()
        {
            if (Instance)
            {
                delete Instance;
            }
        }

        !ManagedObject()
        {
            if (Instance)
            {
                delete Instance;
            }
        }

        T* GetInstance()
        {
            return Instance;
        }

    protected:
        T* Instance;
    };
}