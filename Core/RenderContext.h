#pragma once

#include <Windows.h>
#include "ManagedObject.h"

using namespace System;

namespace Core
{
    class RenderContext
    {
    public:
        RenderContext(HWND handle);
    };
}

namespace OpenGL
{
    public ref class RenderContext : public ManagedObject<Core::RenderContext>
    {
    public:
        RenderContext(IntPtr handle);
    };
}