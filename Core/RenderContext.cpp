#include "RenderContext.h"
#include "GL/glew.h"
#include <thread>

namespace Core
{
    namespace
    {
        void RenderOpenGL(HWND handle)
        {
            HDC hdc = GetDC(handle);

            PIXELFORMATDESCRIPTOR pfd =
            {
                sizeof(PIXELFORMATDESCRIPTOR),
                1,
                PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    //Flags
                PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
                32,                   // Colordepth of the framebuffer.
                0, 0, 0, 0, 0, 0,
                0,
                0,
                0,
                0, 0, 0, 0,
                24,                   // Number of bits for the depthbuffer
                8,                    // Number of bits for the stencilbuffer
                0,                    // Number of Aux buffers in the framebuffer.
                PFD_MAIN_PLANE,
                0,
                0, 0, 0
            };

            int  letWindowsChooseThisPixelFormat;
            letWindowsChooseThisPixelFormat = ChoosePixelFormat(hdc, &pfd);
            SetPixelFormat(hdc, letWindowsChooseThisPixelFormat, &pfd);

            HGLRC Context = wglCreateContext(hdc);
            wglMakeCurrent(hdc, Context);

            glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

            while (true)
            {
                glClear(GL_COLOR_BUFFER_BIT);

                glBegin(GL_POINTS);

                for (size_t n = 0; n < 10000; ++n)
                {
                    constexpr float LO = -1.0f;
                    constexpr float HI = 1.0f;
                    float x = LO + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (HI - LO)));
                    float y = LO + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (HI - LO)));
                    glVertex3f(x, y, 0.0f);
                }

                glEnd();

                wglSwapLayerBuffers(hdc, WGL_SWAP_MAIN_PLANE);
            }    

            wglDeleteContext(Context);
        }
    }

    RenderContext::RenderContext(HWND handle)
    {
        std::thread t1(RenderOpenGL, handle);
        t1.detach();
    }
}

namespace OpenGL
{
    RenderContext::RenderContext(System::IntPtr handle)
        : ManagedObject(new Core::RenderContext(static_cast<HWND>(handle.ToPointer())))
    {
    }
}